#include "../include/bmp.h"

#include <stdint.h>
#include <stdio.h>

#define BFTYPE 0x4D42
#define BIBITCOUNT 24
#define BFRESERVED 0
#define BOFFBITS 54
#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BICOMPRESSION 0
#define BIXPPM 1000
#define BIYPPM 1000
#define BICLRUSED 0
#define BICLSIMPORTANT 0

static int32_t get_padding(uint32_t width) {
    if (width % 4 == 0)
        return 0;
    else
        return (int32_t) (4 - ((width * 3) % 4));
}

enum read_status from_bmp(FILE* inp, struct image* img) {
    struct bmp_header head = {0};

    if (fread(&head, sizeof (struct bmp_header), 1, inp) != 1)
        return READ_INVALID_HEADER;

    if (head.bfType != BFTYPE)
        return READ_INVALID_SIGNATURE;

    if (head.biBitCount != BIBITCOUNT)
        return READ_INVALID_BITS;

    *img = create_image(head.biWidth, head.biHeight);
    int32_t padding = get_padding(img->width);

    fseek(inp, head.bOffBits, SEEK_SET);;
    for (size_t h = 0; h < img->height; ++h) {
        fread(
                img->data + h * (img->width),
                (size_t) (img->width) * sizeof (struct pixel),
                1, inp);
        fseek(inp, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image* img) {
    int32_t padding = get_padding(img->width);

    struct bmp_header head = {
            .bfType = BFTYPE,
            .bfReserved = BFRESERVED,
            .bOffBits = BOFFBITS,
            .biSize = BISIZE,
            .biPlanes = BIPLANES,
            .biBitCount = BIBITCOUNT,
            .biCompression = BICOMPRESSION,
            .biXPelsPerMeter = BIXPPM,
            .biYPelsPerMeter = BIYPPM,
            .biClrUsed = BICLRUSED,
            .biClrImportant = BICLSIMPORTANT,
            .biWidth = img->width,
            .biHeight = img->height,
            .biSizeImage = (sizeof (struct pixel) * img->width + padding) * img->height,
            .bfileSize = sizeof (struct bmp_header) + (sizeof (struct pixel) * img->width + padding) * img->height,
    };

    if (fwrite(&head, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;

    uint8_t placeholder[8] = {0};
    for (uint32_t i = 0; i < img->height; ++i) {
        if (fwrite(
                img->data + i * img->width,
                img->width * sizeof (struct pixel),
                1, out) != 1)
            return WRITE_ERROR;

        if (fwrite(placeholder, padding, 1, out) != 1)
            return WRITE_ERROR;
    }

    return WRITE_OK;
}
