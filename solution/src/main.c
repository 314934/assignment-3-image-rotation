#include "bmp.h"
#include "image.h"
#include "rotate.h"

#include <stdio.h>

int main( int argc, char** argv ) {

    if(argc != 3) {
        return 1;
    }

    FILE *inp = fopen(argv[1], "rb");
    if (!inp)
        return 1;

    struct image img = {0};

    enum read_status rstatus = from_bmp(inp, &img);


    if (rstatus != READ_OK)
        return 1;

    if (fclose(inp) != 0)
        return 1;

    struct image rotated = create_image(img.height, img.width);
    rotate(&img, &rotated);
    image_free(&img);

    FILE *out = fopen(argv[2], "wb");

    if (!out)
        return 1;

    enum write_status wstatus = to_bmp(out, &rotated);

    if (wstatus != WRITE_OK)
        return 1;

    if(fclose(out) != 0)
        return 1;

    image_free(&rotated);

    return 0;
}
