#include "image.h"

struct image create_image(uint64_t width, uint64_t height) {
    return (struct image) {
        .height = height,
        .width = width,
        .data = malloc((sizeof (struct pixel)) * width * height)
    };
}

void image_free(struct image* img){
    free(img->data);
}
