#include "rotate.h"

void rotate(struct image* img, struct image* rotated) {
    for (size_t h = 0; h < img->height; h++) {
        for (size_t w = 0; w < img->width; w++) {
            size_t new_index = img->height - 1 - h + w * rotated->width;
            size_t prev_index = w + h * img->width;
            rotated->data[new_index] = img->data[prev_index];
        }
    }
}
