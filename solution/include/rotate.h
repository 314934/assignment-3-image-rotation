#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"

void rotate(struct image* img, struct image* rotated);

#endif
