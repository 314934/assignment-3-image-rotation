#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel {
    uint8_t r, g, b;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image(uint64_t width,uint64_t height);
void image_free(struct image* img);

#endif
